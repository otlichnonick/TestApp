//
//  ContentView.swift
//  TestApp
//
//  Created by Anton Agafonov on 15.07.2022.
//

import SwiftUI

struct ContentView: View {
    @State var selection = 0
    @State var textIsSmall = false
    
    var body: some View {
        TabView(selection: $selection) {
            LayoutScreen(textIsSmall: $textIsSmall)
                .tag(0)
                .tabItem {
                    Text("Layout")
                }
            
            ControlsScreen(textIsSmall: $textIsSmall)
                .tag(1)
                .tabItem {
                    Text("Controls")
                }
            
            NavigationScreen(selection: $selection)
                .tag(2)
                .tabItem {
                    Text("Navigation")
                }
            
            SettingsScreen()
                .tag(3)
                .tabItem {
                    Text("Settings")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  SettingsScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 18.07.2022.
//

import SwiftUI
import UIKit

struct SettingsScreen: View {
    @State var navLinkIsActive: Bool = false
    @State var navTitle: String = ""
    
    var body: some View {
        UITableView.appearance().backgroundColor = .clear
        
        return NavigationView {
            VStack(spacing: 0) {
                HStack {
                    Text("Настройки")
                        .font(.title)
                        .fontWeight(.bold)
                    
                    Spacer()
                }
                .padding()
                
                List {
                    Section {
                        uresInfoCell
                    }
                    
                    Section {
                        settingsCells
                    }
                }
                .listStyle(.insetGrouped)
                
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 10) {
                        ForEach(Constants.emojies, id: \.self) { emoji in
                            ScrollViewCell(emoji: emoji)
                        }
                    }
                }
                .padding(.horizontal)
                
                Spacer()
            }
            .background(Color(uiColor: .systemGray6))
            .navigationBarHidden(true)
            .background(
                NavigationLink(isActive: $navLinkIsActive, destination: {
                    SettingsDetailScreen(navTitle: navTitle)
                }, label: {
                    EmptyView()
                })
            )
        }
    }
}

extension SettingsScreen {
    var uresInfoCell: some View {
        Button {
            navTitle = "John Appleseed"
            navLinkIsActive.toggle()
        } label: {
            HStack {
                ZStack {
                Circle()
                    .frame(width: 40, height: 40)
                    .foregroundColor(.gray)
                    
                    Text("JA")
                        .foregroundColor(.white)
                }
                
                VStack(alignment: .leading) {
                    HStack {
                        Text("John Appleseed")
                    }
                    
                    Text("Apple ID, iCloud, контент и покупки")
                        .font(.caption2)
                }
                .badge("›")
                .foregroundColor(.black)
            }
        }
    }
    
    var settingsCells: some View {
        ForEach(Constants.settings, id: \.0) { item in
            Label(item.0, systemImage: item.1)
                .badge("›")
                .onTapGesture {
                    navTitle = item.0
                    navLinkIsActive.toggle()
                }
        }
    }
}

struct SettingsScreen_Previews: PreviewProvider {
    static var previews: some View {
        SettingsScreen()
    }
}


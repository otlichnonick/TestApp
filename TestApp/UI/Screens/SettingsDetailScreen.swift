//
//  SettingsDetailScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 18.07.2022.
//

import SwiftUI

struct SettingsDetailScreen: View {
    let navTitle: String
    
    var body: some View {
        ZStack {
            Color(uiColor: .systemGray6)
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                Text("Это экран с настройками")
            }
            .navigationTitle(navTitle)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct SettingsDetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        SettingsDetailScreen(navTitle: "Settings")
    }
}

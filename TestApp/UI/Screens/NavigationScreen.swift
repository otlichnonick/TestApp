//
//  NavigationScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 15.07.2022.
//

import SwiftUI

struct NavigationScreen: View {
    @State var navLinkRoute: NavLinkRoute = .detailScreen
    @State var showActionSheet = false
    @State var showFullScreenCover = false
    @State var navLinkIsActive = false
    @State var currentColor = Color.yellow
    @Binding var selection: Int
    
    var body: some View {
        NavigationView {
            VStack(spacing: 30) {
                VStack(alignment: .center, spacing: 30) {
                    CustomButton(title: "Показать ActionSheet") {
                        showActionSheet.toggle()
                    }
                
                    CustomButton(title: "Показать FullScreenCover") {
                        showFullScreenCover.toggle()
                    }
                
                    CustomButton(title: "Перейти на новый экран") {
                        navLinkRoute = .detailScreen
                        navLinkIsActive.toggle()
                    }
                }
                .padding(.top)
                
                Circle()
                    .frame(width: 200, height: 200, alignment: .center)
                    .foregroundColor(currentColor)
                    .animation(.easeIn, value: currentColor)
                
                CustomButton(title: "Посмотреть список новостей") {
                    navLinkRoute = .newsScreen
                    navLinkIsActive.toggle()
                }
                
                Spacer()
            }
            .navigationBarHidden(true)
            .background(
                NavigationLink(isActive: $navLinkIsActive, destination: {
                    navigateTo(navLinkRoute)
                }, label: {
                    EmptyView()
                })
            )
            .actionSheet(isPresented: $showActionSheet) {
                ActionSheet(title: Text("Выбери цвет"), buttons: [
                    .default(Text("Желтый"), action: {
                        currentColor = .yellow
                    }),
                    .default(Text("Красный"), action: {
                        currentColor = .red
                    }),
                    .default(Text("Синий"), action: {
                        currentColor = .blue
                    }),
                    .cancel()
                ])
            }
            .fullScreenCover(isPresented: $showFullScreenCover) {
                FullScreenCover(showFullScreenCover: $showFullScreenCover)
            }
        }
    }
    
    @ViewBuilder
    func navigateTo(_ navLinkRoute: NavLinkRoute) -> some View {
        switch navLinkRoute {
        case .newsScreen:
            NewsScreen()
        case .detailScreen:
            DetailScreen(selection: $selection)
        }
    }
}

struct NavigationScreen_Previews: PreviewProvider {
    static var previews: some View {
        NavigationScreen(selection: .constant(1))
    }
}

//
//  ControlsScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 15.07.2022.
//

import SwiftUI

struct ControlsScreen: View {
    @State var text = ""
    @State var selectedSegment = 0
    @Binding var textIsSmall: Bool
    @State var showAlert = false
    private var buttonWidth: CGFloat {
        UIScreen.main.bounds.width * 0.4
    }
    
    var body: some View {
        VStack(spacing: 20) {
            TextField("Текстовое поле", text: $text)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.top)
            
            Text(text)
                .font(.body)
                .multilineTextAlignment(.center)
                .frame(height: 30)
            
            HStack {
                CustomButton(buttonWidth: buttonWidth, title: "Увеличить") {
                    if textIsSmall {
                    textIsSmall = false
                        showAlert.toggle()
                    }
                }
                
                Spacer()

                CustomButton(buttonWidth: buttonWidth, title: "Уменьшить") {
                    if !textIsSmall {
                    textIsSmall = true
                        showAlert.toggle()
                    }
                }
            }
            
            Picker("", selection: $selectedSegment) {
                Text("Красный")
                    .tag(0)
                
                Text("Желтый")
                    .tag(1)
            }
            .pickerStyle(SegmentedPickerStyle())
            
            RoundedRectangle(cornerRadius: 15)
                .frame(height: 200)
                .foregroundColor(selectedSegment == 0 ? .red : .yellow)
            
            Spacer()            
        }
        .alert(isPresented: $showAlert) {
            Alert(title: Text("Размер текста изменен"), dismissButton: .default(Text("Хорошо")))
        }
        .padding(.horizontal)
    }
}

struct ControlsScreen_Previews: PreviewProvider {
    static var previews: some View {
        ControlsScreen(textIsSmall: .constant(true))
    }
}

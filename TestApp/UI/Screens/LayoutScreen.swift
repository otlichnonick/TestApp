//
//  LayoutScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 15.07.2022.
//

import SwiftUI

struct LayoutScreen: View {
    @Binding var textIsSmall: Bool
    @State var degrees: CGFloat = 0
    @State var alignment: Alignment = .topLeading
    
    var body: some View {
        VStack {
            ZStack(alignment: alignment) {
                Color.red

                buildText()
                    .padding()
                    .transition(AnyTransition.opacity.animation(.easeIn(duration: 1)))
                    .onTapGesture {
                        withAnimation(.easeIn(duration: 1)) {
                            moveText()
                        }
                    }
            }
            .cornerRadius(15)
            
            GeometryReader { geometry in
                let viewWidth = degrees == 90 ? geometry.size.height/2 : geometry.size.width/2
                let viewHeight = degrees == 0 ? geometry.size.height : geometry.size.width
                
                HStack(spacing: 0) {
                    VStack {
                        Text("а это VStack, текст сверху")
                            .padding()
                        
                        Spacer()
                    }
                    .frame(width: viewWidth, height: viewHeight)
                    .background(Color.yellow)
                    
                    VStack {
                        Spacer()
                        
                        Text("а это VStack, текст снизу")
                            .padding()
                    }
                    .frame(width: viewWidth, height: viewHeight)
                    .background(Color.green)
                }
                .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                .rotationEffect(.degrees(degrees))
                .clipped()
                .onTapGesture {
                    withAnimation {
                        rotate()
                    }
                }
            }
        }
        .font(textIsSmall ? .body : .largeTitle)
    }
    
    @ViewBuilder
    private func buildText() -> some View {
        switch alignment {
        case .topLeading:
            Text("это ZStack с выравниванием текста topLeading")
                .multilineTextAlignment(.leading)
        case .bottomLeading:
            Text("это ZStack с выравниванием текста bottomLeading")
                .multilineTextAlignment(.leading)
        case .bottomTrailing:
            Text("это ZStack с выравниванием текста bottomTrailing")
                .multilineTextAlignment(.trailing)
        default:
            Text("это ZStack с выравниванием текста topTrailing")
                .multilineTextAlignment(.trailing)
        }
    }
    
    private func rotate() {
        degrees = degrees == 0 ? 90 : 0
    }
    
    private func moveText() {
        switch alignment {
        case .topLeading:
            alignment = .bottomLeading
        case .bottomLeading:
            alignment = .bottomTrailing
        case .bottomTrailing:
            alignment = .topTrailing
        default:
            alignment = .topLeading
        }
    }
}

struct LayoutScreen_Previews: PreviewProvider {
    static var previews: some View {
        LayoutScreen(textIsSmall: .constant(true))
    }
}

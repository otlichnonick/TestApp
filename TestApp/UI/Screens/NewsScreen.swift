//
//  NewsScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import SwiftUI

struct NewsScreen: View {
    @StateObject var newsViewModel = NewsViewModel()
    
    var body: some View {
        VStack {
            Picker("", selection: $newsViewModel.selectedSegment) {
                Text("Все новости")
                    .tag(0)
                Text("Главное")
                    .tag(1)
            }
            .padding(.horizontal)
            .pickerStyle(SegmentedPickerStyle())
            .onChange(of: newsViewModel.selectedSegment) { _ in
                if newsViewModel.list.isEmpty {
                    newsViewModel.getNews()
                }
            }
            
            ZStack {
                ScrollView {
                    LazyVStack(spacing: 12) {
                        ForEach(newsViewModel.list) { newsItem in
                            Button {
                                newsViewModel.selectedNews = newsItem
                                newsViewModel.navLinkIsActive.toggle()
                            } label: {
                                NewsCell(cellData: newsItem)
                                    .onAppear {
                                        if newsItem.uuid == newsViewModel.list.last?.uuid {
                                            newsViewModel.getNews()
                                        }
                                    }
                            }
                        }
                    }
                }
                
                if newsViewModel.showProgressView {
                    CustomProgressView()
                }
            }
        }
        .navigate(using: $newsViewModel.selectedNews, destination: { selectedNews in
            DetailNewsScreen(news: selectedNews)
        })
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            newsViewModel.getNews()
        }
    }
}

struct NewsScreen_Previews: PreviewProvider {
    static var previews: some View {
        NewsScreen()
    }
}

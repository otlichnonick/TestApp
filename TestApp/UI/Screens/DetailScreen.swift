//
//  DetailScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 15.07.2022.
//

import SwiftUI

struct DetailScreen: View {
    @Binding var selection: Int
    
    var body: some View {
        ContainerView {
            VStack(spacing: 50) {
                Text("Hello, World!")
                
                Text("Это может быть экран с детальной информацией о чем-нибудь")
                    .multilineTextAlignment(.center)
                
                CustomButton(title: "Перейти в Controls") {
                    selection = 1
                }
            }
            .navigationBarHidden(true)
        }
    }
}

struct DetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        DetailScreen(selection: .constant(1))
    }
}

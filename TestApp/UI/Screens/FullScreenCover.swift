//
//  FullScreenCover.swift
//  TestApp
//
//  Created by Anton Agafonov on 18.07.2022.
//

import SwiftUI

struct FullScreenCover: View {
    @Binding var showFullScreenCover: Bool
    
    var body: some View {
        ZStack {
            Color.gray
            VStack {
                HStack {
                    Button {
                        showFullScreenCover.toggle()
                    } label: {
                        Image(systemName: "x.circle")
                            .foregroundColor(.red)
                            .symbolRenderingMode(.palette)
                    }
                    .frame(width: 40, height: 40)
                    
                    Spacer()
                }
                
                Spacer()
                
                Text("""
Это модальное представление со своей иерархией вью во весь экран, safe area не игнорируется, поэтому не залита серым цветом

А еще обрати внимание - в SwiftUI есть возможность раскрашивать SFSymbols 😍, а в UIKit - нельзя 😛
""")
                .padding()
                .font(.title)
                
                Spacer()
            }
        }
    }
}

struct FullScreenCover_Previews: PreviewProvider {
    static var previews: some View {
        FullScreenCover(showFullScreenCover: .constant(true))
    }
}

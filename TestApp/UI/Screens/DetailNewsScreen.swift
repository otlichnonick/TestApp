//
//  DetailNewsScreen.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailNewsScreen: View {
    let news: DataModel
    
    private var url: URL? {
        URL(string: news.image_url ?? "")
    }
    
    var body: some View {
        ScrollView {
            VStack(spacing: 16) {
                WebImage(url: url)
                    .cancelOnDisappear(false)
                    .resizable()
                    .placeholder {
                        Image(systemName: "photo.fill")
                            .renderingMode(.template)
                    }
                    .indicator(.activity)
                    .aspectRatio(contentMode: .fill)
                
                VStack {
                    HStack {
                        Text(news.title)
                            .font(.headline)
                        
                        Spacer()
                    }
                    
                    HStack {
                        Text(news.description ?? "")
                            .font(.body)
                        
                        Spacer()
                    }
                }
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)
            }
            .padding(.horizontal, 10)
        }
    }
}

struct DetailNewsScreen_Previews: PreviewProvider {
    static var previews: some View {
        DetailNewsScreen(news: DataModel())
    }
}

//
//  CustomButton.swift
//  TestApp
//
//  Created by Anton Agafonov on 18.07.2022.
//

import SwiftUI

struct CustomButton: View {
    var buttonWidth: CGFloat?
    let title: String
    let callback: () -> Void
    
    var body: some View {
        Button { callback() } label: {
            Text(title)
                .font(.body)
                .foregroundColor(.white)
                .padding(8)
                .frame(width: buttonWidth != nil ? buttonWidth : nil)
                .background(Color.green)
                .cornerRadius(16)
                .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .strokeBorder(Color.black, lineWidth: 2, antialiased: true)
                )
        }
    }
}

struct CustomButton_Previews: PreviewProvider {
    static var previews: some View {
        CustomButton(title: "title", callback: {})
    }
}

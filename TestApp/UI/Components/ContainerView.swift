//
//  ContainerView.swift
//  TestApp
//
//  Created by Anton Agafonov on 18.07.2022.
//

import SwiftUI

struct ContainerView<Content: View>: View {
    @Environment(\.presentationMode) var presentationMode
    var content: Content
    
    init(@ViewBuilder content: @escaping () -> Content) {
        self.content = content()
    }
    
    var body: some View {
        VStack {
            HStack {
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20, alignment: .center)
                        .foregroundColor(.black)
                }
                
                Spacer()
            }
            .padding(8)
            
            content
            
            Spacer()
        }
        .background(
        Image("background")
            .imageStyle()
        )
        .navigationBarBackButtonHidden(true)
    }
}

struct ContainerView_Previews: PreviewProvider {
    static var previews: some View {
        ContainerView {
            EmptyView()
        }
    }
}

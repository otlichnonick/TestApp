//
//  ScrollViewCell.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import SwiftUI

struct ScrollViewCell: View {
    let emoji: String
    
    var body: some View {
        Text(emoji)
            .font(.system(size: 80))
            .frame(width: UIScreen.main.bounds.width * 0.5, height: UIScreen.main.bounds.width * 0.5)
            .background(LinearGradient(colors: [.yellow, .gray, .indigo], startPoint: .top, endPoint: .bottomTrailing))
            .cornerRadius(30)
            
    }
}

struct ScrollViewCell_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewCell(emoji: "🦊")
    }
}

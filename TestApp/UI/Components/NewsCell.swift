//
//  NewsCell.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import SwiftUI

struct NewsCell: View {
    let cellData: DataModel
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(cellData.title)
                    .font(.title3)
                
                Text(cellData.description ?? "")
                    .font(.footnote)
            }
            .multilineTextAlignment(.leading)
            .fixedSize(horizontal: false, vertical: true)
            .foregroundColor(.black)
            
            Spacer()
        }
        .padding()
        .background(Color(uiColor: .systemGray6))
        .cornerRadius(10)
        .padding(.horizontal)
    }
}


struct NewsCell_Previews: PreviewProvider {
    static var previews: some View {
        NewsCell(cellData: DataModel())
    }
}

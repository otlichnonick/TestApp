//
//  NewsService.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation

class NewsService: BasePresenter {
    private let networkManager: APIManager = .init()
    static let shared: NewsService = .init()
    
    func getAllNews(queryParams: [String: String], handler: @escaping (Result<NewsModel, Error>) -> Void) {
        let publisher = networkManager.getAllNews(queryParams: queryParams)
        baseRequest(publisher: publisher, handler: handler)
    }
    
    func getTopNews(queryParams: [String: String], handler: @escaping (Result<NewsModel, Error>) -> Void) {
        let publisher = networkManager.getTopNews(queryParams: queryParams)
        baseRequest(publisher: publisher, handler: handler)
    }
}

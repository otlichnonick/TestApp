//
//  APICall.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias HTTPQuery = [String: String]

protocol APICall {
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
}

extension APICall {
    func urlRequest(baseUrl: String) throws -> URLRequest {
        guard let url = URL(string: baseUrl + path) else {
            throw ErrorHandler.notValidURL
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        request.httpBody = nil
        return request
    }
}

//
//  APIManagerProtocol.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation
import Combine

protocol APIManagerProtocol {
    var baseURL: String { get }
}

extension APIManagerProtocol {
      func fetch<Output: Codable>(endpoint: APICall) -> AnyPublisher<Output, Error> {
          do {
              let request = try endpoint.urlRequest(baseUrl: baseURL)
              return URLSession.shared.dataTaskPublisher(for: request)
                  .tryMap { result -> Output in
                      let httpResponse = result.response as? HTTPURLResponse
                      if httpResponse?.statusCode == 204 {
                          throw ErrorHandler.noContent
                      }
                      
                      return try ErrorHandler.checkDecodingErrors(model: Output.self, with: result.data)
                  }
                  .eraseToAnyPublisher()
          } catch {
              return AnyPublisher(Fail<Output, Error>(error: ErrorHandler.notValidURL))
          }
      }
  }

struct APIManager: APIManagerProtocol {
    var baseURL: String = Constants.baseUrl
    
    func getAllNews(queryParams: [String: String]) -> AnyPublisher<NewsModel, Error> {
        return fetch(endpoint: API.getAllNews(queryParams))
    }
    
    func getTopNews(queryParams: [String: String]) -> AnyPublisher<NewsModel, Error> {
        return fetch(endpoint: API.getTopNews(queryParams))
    }
}

extension APIManager {
    enum API {
        case getAllNews([String: String])
        case getTopNews([String: String])
    }
}

extension APIManager.API : APICall {
    var path: String {
        switch self {
        case .getAllNews(let queryParams):
            return Helpers.buildQuery(path: "all", queryParams: queryParams)
        case .getTopNews(let queryParams):
            return Helpers.buildQuery(path: "top", queryParams: queryParams)
        }
    }
    
    var method: HTTPMethod {
        return .GET
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    
}

//
//  NewsModel.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation

struct NewsModel: Codable {
    var data: [DataModel] = .init()
}

struct DataModel: Codable, Equatable, Identifiable {
    var uuid: String = ""
    var title: String = ""
    var description: String?
    var image_url: String?
    
    var id: String {
        uuid
    }
}

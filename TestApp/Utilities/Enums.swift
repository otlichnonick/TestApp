//
//  Enums.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation

enum LoadState {
    case notRequest
    case loading
    case success
    case error
}

enum HTTPMethod: String {
    case GET
    case POST
    case PATCH
    case PUT
    case DELETE
}

enum NavLinkRoute {
    case newsScreen
    case detailScreen
}

enum ErrorHandler: Error, LocalizedError {
    case notValidURL
    case notValidBody
    case noContent
    
    public var errorDescription: String? {
        switch self {
        case .notValidURL:
            return "Некорректный URL"
        case .notValidBody:
            return  "Некорректное тело запроса"
        case .noContent:
            return "Нет контента"
        }
    }
    
    static func checkDecodingErrors<Output: Codable>(model: Output.Type, with data: Data) throws -> Output {
        do {
            return try JSONDecoder().decode(model.self, from: data)
        } catch DecodingError.keyNotFound(let key, let context) {
            debugPrint("\(Output.self) could not find key \(key) in JSON: \(context.debugDescription)")
            throw DecodingError.keyNotFound(key, context)
        } catch DecodingError.valueNotFound(let type, let context) {
            debugPrint("\(Output.self) could not find type \(type) in JSON: \(context.debugDescription)")
            throw DecodingError.valueNotFound(type, context)
        } catch DecodingError.typeMismatch(let type, let context) {
            debugPrint("\(Output.self)  type mismatch for type \(type) in JSON: \(context.debugDescription)")
            throw DecodingError.typeMismatch(type, context)
        } catch DecodingError.dataCorrupted(let context) {
            debugPrint("\(Output.self) data found to be corrupted in JSON: \(context.debugDescription)")
            throw DecodingError.dataCorrupted(context)
        }
    }
}

//
//  Helpers.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation

struct Helpers {
    static func buildQuery(path: String, queryParams: [String: String]) -> String {
        var urlComponents = URLComponents()
        urlComponents.path = path
        urlComponents.queryItems = queryParams.map { URLQueryItem(name: $0, value: $1) }
        guard let urlString = urlComponents.url?.absoluteString else { return "" }
        return urlString
    }
}

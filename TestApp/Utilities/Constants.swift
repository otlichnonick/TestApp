//
//  Constants.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation

struct Constants {
    static let apiKey = "cCroHltkP75P6zZCAfErTHkktGE9DvvQdePQPhlz"
    static let baseUrl = "https://api.thenewsapi.com/v1/news/"
    static let emojies: [String] = ["🐥", "🦆", "🪰", "🦊", "🐶", "🐰" ,"🐻‍❄️"]
    static let settings: [(String, String)] = [
        ("Сотовая связь", "antenna.radiowaves.left.and.right"),
        ("Пароли", "key"),
        ("Конфиденциальность", "hand.raised"),
        ("Почта", "envelope"),
        ("Экран и яркость", "textformat.size")
    ]
}

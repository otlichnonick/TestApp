//
//  ImageStyle.swift
//  TestApp
//
//  Created by Anton Agafonov on 18.07.2022.
//

import SwiftUI

extension Image {
    func imageStyle() -> some View {
        self
            .resizable()
            .aspectRatio(contentMode: .fill)
            .edgesIgnoringSafeArea(.top)
    }
}



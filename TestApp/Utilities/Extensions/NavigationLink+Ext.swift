//
//  NavigationLink+Ext.swift
//  TestApp
//
//  Created by Anton Agafonov on 19.07.2022.
//

import Foundation
import SwiftUI

extension NavigationLink where Label == EmptyView {
    init?<Value>(_ binding: Binding<Value?>,
                 @ViewBuilder destination: (Value) -> Destination) {
        guard let value = binding.wrappedValue else { return nil }
        
        let isActive = Binding<Bool>(
            get: { true },
            set: { newValue in
                if !newValue {
                    binding.wrappedValue = nil
                }
            })
        
        self.init(isActive: isActive) {
            destination(value)
        } label: {
            EmptyView()
        }
    }
}

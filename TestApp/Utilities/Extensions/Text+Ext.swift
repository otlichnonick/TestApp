//
//  Text+Ext.swift
//  TestApp
//
//  Created by Anton Agafonov on 25.07.2022.
//

import SwiftUI

extension Text {
    func widgetStyle() -> some View {
        self
            .font(.footnote)
            .padding(.horizontal, 6)
            .foregroundColor(.white)
            .shadow(color: .black, radius: 1)
    }
}


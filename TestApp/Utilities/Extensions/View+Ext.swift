//
//  View+Ext.swift
//  TestApp
//
//  Created by Anton Agafonov on 20.07.2022.
//

import Foundation
import SwiftUI

extension View {
    @ViewBuilder
    func navigate<Value, Destination: View>(using binding: Binding<Value?>, @ViewBuilder destination: (Value) -> Destination) -> some View {
        background(NavigationLink(binding, destination: destination))
    }
}

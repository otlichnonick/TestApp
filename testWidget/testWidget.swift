//
//  testWidget.swift
//  testWidget
//
//  Created by Anton Agafonov on 25.07.2022.
//

import WidgetKit
import SwiftUI

fileprivate let mockNews = [
    DataModel(uuid: "1", title: "Travel chaos: No end in sight to long Dover queues, holidaymakers warned", description: nil, image_url: nil),
    DataModel(uuid: "2", title: "The Chinese have started actively buying food with an expiring expiration date", description: nil, image_url: nil),
    DataModel(uuid: "3", title: "Greece will demand ", description: nil, image_url: nil)
]

struct Provider: TimelineProvider {
    
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), type: .placeholder)
    }
    
    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), type: .snapshot)
        completion(entry)
    }
    
    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        NewsService.shared.getAllNews(queryParams: ["api_token": Constants.apiKey, "page": "1", "language": "en"]) { result in
            switch result {
            case .success(let response):
                completion(Timeline(entries: [SimpleEntry(date: Date(), type: .banner(response.data))], policy: .atEnd))
            case .failure(_):
                completion(Timeline(entries: [SimpleEntry(date: Date(), type: .snapshot)], policy: .never))
            }
        }
    }
}
struct SimpleEntry: TimelineEntry {
    enum EntryType {
        case placeholder
        case snapshot
        case banner([DataModel])
    }
    let date: Date
    let type: EntryType
}

@main
struct testWidget: Widget {
    let kind: String = "testWidget"
    
    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            testWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
        .supportedFamilies([.systemSmall, .systemMedium])
    }
}

struct testWidget_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            testWidgetEntryView(entry: SimpleEntry(date: Date(), type: .snapshot))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
            testWidgetEntryView(entry: SimpleEntry(date: Date(), type: .snapshot))
                .previewContext(WidgetPreviewContext(family: .systemMedium))
        }
    }
}

struct testWidgetEntryView : View {
    var entry: Provider.Entry
    
    @ViewBuilder
    var body: some View {
        switch entry.type {
        case .placeholder:
            PlaceholderView()
        case .snapshot:
            SnapshotView()
        case .banner(let newsList):
            BannerView(newsList: newsList)
        }
    }
}

struct PlaceholderView: View {
    @Environment(\.widgetFamily) var widgetFamily
    
    @ViewBuilder
    var body: some View {
        ZStack {
            LinearGradient(colors: [.gray, .cyan, .gray], startPoint: .top, endPoint: .bottom)
            
            VStack {
                switch widgetFamily {
                case .systemSmall:
                    Text(mockNews.first!.title)
                        .widgetStyle()
                        .padding(.top, 6)

                default:
                    VStack(alignment: .leading, spacing: 2) {
                        ForEach(mockNews, id: \.self.uuid) { item in
                            Text(item.title)
                                .widgetStyle()
                        }
                    }
                    .padding(.top, 6)

                }
                
                Spacer()
                
                CustomButton(title: "See all news") {}
                    .padding(.bottom, 6)
            }
        }
    }
}

struct SnapshotView: View {
    @Environment(\.widgetFamily) var widgetFamily
    
    @ViewBuilder
    var body: some View {
        GeometryReader { geo in
            VStack {
                switch widgetFamily {
                case .systemSmall:
                    Text(mockNews.first!.title)
                        .widgetStyle()
                        .padding(.top, 6)
                        .frame(width: geo.size.width)
                    
                default:
                    VStack(alignment: .leading, spacing: 2) {
                        ForEach(mockNews, id: \.self.uuid) { item in
                            HStack {
                            Text(item.title)
                                .widgetStyle()
                                
                                Spacer()
                            }
                            .frame(width: geo.size.width)
                        }
                    }
                    .padding(.top, 6)
                }
                
                Spacer()
                
                CustomButton(title: "See all news") {}
                    .padding(.bottom, 6)
            }
            .background(
                Image("newsWidgetBackground")
                    .resizable()
                    .scaledToFill()
            )
        }
    }
}

struct BannerView: View {
    @Environment(\.widgetFamily) var widgetFamily
    let newsList: [DataModel]
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                switch widgetFamily {
                case .systemSmall:
                    Text(newsList.first?.title ?? mockNews.first!.title)
                        .widgetStyle()
                        .padding(.top, 6)
                    
                default:
                    VStack(alignment: .leading, spacing: 2) {
                        ForEach(newsList.prefix(3), id: \.self.uuid) { item in
                            HStack {
                            Text(item.title)
                                .widgetStyle()
                                
                                Spacer()
                            }
                            .frame(width: geo.size.width)
                        }
                    }
                    .padding(.top, 6)
                }
                
                Spacer()
                
                CustomButton(title: "See all news") {}
                    .padding(.bottom, 6)
            }
            .background(
                Image("newsWidgetBackground")
                    .resizable()
                    .scaledToFill()
            )
        }
    }
}
